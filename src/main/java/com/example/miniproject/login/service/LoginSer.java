package com.example.miniproject.login.service;

import com.example.miniproject.login.model.entity.Account;
import com.example.miniproject.login.model.entity.TodoList;
import com.example.miniproject.login.repository.LoginRepo;
import com.example.miniproject.login.repository.database.TodoListDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class LoginSer {
    @Autowired
    LoginRepo loginRepo;

    @Autowired
    TodoListDB todoListDB;

    public Account loginQuery(String email, String password){
        return loginRepo.loginQuery(email, password);
    }


    public TodoList get(Integer id){
        Optional<TodoList> result = todoListDB.findById(id);
        if(result.isPresent()){
            return result.get();
        }else {
            return null;
        }
    }

    public void delete(Integer id){
        Long count = todoListDB.countTodoListById(id);
        if(count == null || count ==0){

        }
        todoListDB.deleteById(id);
    }
}
