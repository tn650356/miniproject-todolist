package com.example.miniproject.login.model.entity;

public interface AccTodoListDB {
    int getId();
    String getEmail();
    String getTodo_name();
    String getNote();
}
