package com.example.miniproject.login.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account {
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private int id;
    private String email;
    private String password;
    private String firstname;
    private String lastname;

    public Account(String email, String password, String firstname, String lastname) {
    }

    public Account(String email, String password) {
    }
}
