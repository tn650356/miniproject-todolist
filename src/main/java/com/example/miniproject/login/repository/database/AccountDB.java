package com.example.miniproject.login.repository.database;

import com.example.miniproject.login.model.entity.AccTodoListDB;
import com.example.miniproject.login.model.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AccountDB extends JpaRepository<Account, Integer> {
    @Query(value = "select *from account where email =?1 and password =?2", nativeQuery = true)
    Account loginQuery(String email, String password);

//    @Query( value = "select * from account join todolist where account.email = todolist.email and account.email='toenahi2k@gmail.com'", nativeQuery = true)
//    List<AccTodoListDB>findAccTodoList(int id);

}
