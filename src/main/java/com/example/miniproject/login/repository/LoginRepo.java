package com.example.miniproject.login.repository;

import com.example.miniproject.login.model.entity.AccTodoListDB;
import com.example.miniproject.login.model.entity.Account;
import com.example.miniproject.login.model.entity.TodoList;
import com.example.miniproject.login.repository.database.AccountDB;
import com.example.miniproject.login.repository.database.TodoListDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class LoginRepo {
    @Autowired
    EntityManager entityManager;

    @Autowired
    AccountDB accountDB;

    @Autowired
    TodoListDB todoListDB;

    public int register(Account account) {
        EntityManager entity = entityManager.getEntityManagerFactory().createEntityManager();
        try {
            entity.getTransaction().begin();
            String sql = "insert into account (email,password,firstname,lastname) values(:email, :password, :firstname, :lastname)";
            Query query = entity.createNativeQuery(sql, Account.class);
            query.setParameter("email", account.getEmail());
            query.setParameter("password", account.getPassword());
            query.setParameter("firstname", account.getFirstname());
            query.setParameter("lastname", account.getLastname());
            query.executeUpdate();
            Query query1 = entity.createNativeQuery("select  LAST_INSERT_ID()");
            int id = Integer.parseInt(query1.getSingleResult().toString());
            entity.getTransaction().commit();
            return id;
        } catch (Exception e) {
            entity.getTransaction().rollback();
        }
        return -1;
    }

    public Account loginQuery(String email, String password) {
        return accountDB.loginQuery(email, password);
    }

//    public List<AccTodoListDB> findAccTodoList(int id) {
//        return accountDB.findAccTodoList(id);
//    }
    public List<TodoList> doLists() {
        return todoListDB.findAll();
    }

    public int addlist(TodoList todoList) {
        EntityManager entity = entityManager.getEntityManagerFactory().createEntityManager();
        try {
            entity.getTransaction().begin();
            String sql = "insert into todolist (todo_name,note,account_id, duetime) values(:todo_name, :note,:account_id, :duetime)";
            Query query = entity.createNativeQuery(sql, TodoList.class);
            query.setParameter("todo_name", todoList.getTodo_name());
            query.setParameter("note", todoList.getNote());
            query.setParameter("account_id", todoList.getAccount_id());
            query.setParameter("duetime", todoList.getDuetime());
            query.executeUpdate();
            Query query1 = entity.createNativeQuery("select  LAST_INSERT_ID()");
            int id = Integer.parseInt(query1.getSingleResult().toString());
            entity.getTransaction().commit();
            return id;
        } catch (Exception e) {
            entity.getTransaction().rollback();
        }
        return -1;
    }




//    public TodoList findById(Long id) {
//        return null;
//    }
}
