package com.example.miniproject.login.repository.database;

import com.example.miniproject.login.model.entity.TodoList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TodoListDB extends JpaRepository<TodoList,Long> {
    Optional<TodoList> findById(Integer id);

    public Long countTodoListById(Integer id);

    void deleteById(Integer id);
}
