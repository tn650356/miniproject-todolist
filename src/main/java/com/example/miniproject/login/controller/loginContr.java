package com.example.miniproject.login.controller;

import com.example.miniproject.login.model.entity.Account;
import com.example.miniproject.login.model.entity.TodoList;
import com.example.miniproject.login.repository.LoginRepo;
import com.example.miniproject.login.repository.database.TodoListDB;
import com.example.miniproject.login.service.LoginSer;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
public class loginContr {

    @Autowired
    LoginRepo loginRepo;
    @Autowired
    LoginSer loginSer;
    @Autowired
    TodoListDB todoListDB;


    @GetMapping("/home")
    public String login(@RequestParam(value = "email", required = false) String email,
                        @RequestParam(value = "password", required = false) String password,
                        Model model) throws NotFoundException {
        Account account = new Account(email, password);
        model.addAttribute("account", account);
        model.addAttribute("email", account.getEmail());
        model.addAttribute("password", account.getPassword());
        return "home";
    }

    @GetMapping("/signup")
    public String signup(@RequestParam(value = "email", required = false) String email,
                         @RequestParam(value = "password", required = false) String password,
                         @RequestParam(value = "firstname", required = false) String firstname,
                         @RequestParam(value = "lastname", required = false) String lastname,
                         Model model) {
        Account account = new Account(email, password, firstname, lastname);
        model.addAttribute("account", account);
        return "signup";
    }

    @PostMapping("/process_signup")
    public String process_signup(Account account, Model model) {
        model.addAttribute("loginResult", "Sucessfully !");
        loginRepo.register(account);
        return "login";
    }

    @PostMapping("/process_login")
    public String process_login(@RequestParam(value = "email", required = false) String email,
                                @RequestParam(value = "password", required = false) String password,
                                Model model) {
        Account account = new Account(email, password);
        model.addAttribute("account", account);
        model.addAttribute("email", account.getEmail());
        model.addAttribute("password", account.getPassword());
        Account account1 = loginSer.loginQuery(email, password);
        if (account1 == null) {
            model.addAttribute("loginResult", "Check your information !");
            return "login";
        }
        return "todolist";
    }

    @GetMapping("/addtodolist")
    public String addTodolist(@RequestParam(value = "todo_name", required = false) String todo_name,
                              @RequestParam(value = "note", required = false) String note,
                              @RequestParam(value = "duetime", required = false) String duetime,
                              Model model) {
        TodoList todoList = new TodoList(todo_name, note, duetime);
        model.addAttribute("todoList", todoList);
        return "addtodolist";
    }

    @PostMapping("/add_list")
    public String add_list(TodoList todoList, Model model) {
        loginRepo.addlist(todoList);
        return "todolist";
    }

    @GetMapping ( "/todolist")
    public String Lists(Model model) {
        List<TodoList> Lists = todoListDB.findAll();
        model.addAttribute("Lists", Lists);
        return "todolist";
    }

    @GetMapping("/getList/{id}")
    public String getList(@PathVariable("id") Integer id, Model model, RedirectAttributes ra){
        try {
            TodoList todoList = loginSer.get(id);
            model.addAttribute("todoList", todoList);
            return "addtodolist";
        }catch (Exception e){
            ra.addFlashAttribute("message","No");
            return "todolist";
        }
    }
    @GetMapping("/deleteList/{id}")
    public String deleteList(@PathVariable("id") Integer id, RedirectAttributes ra){
        try {
            loginSer.delete(id);
        }catch (Exception e){
            ra.addFlashAttribute("message","No");
        }
        return "todolist";
    }
}
